package test.data_structures;


import static org.junit.Assert.*;
import org.junit.Test; 	  
import model.data_structures.Nodo; 
import model.data_structures.Pila; 

public class TestPila <T extends Comparable<T>> { 

	private Pila<T> pila; 
	private int tamanio; 
	private Nodo<T> top; 


	@Test 
	public void setUpEscenario1() { 
		pila= new Pila<T>(); 
	} 

	@Test 
	public void testPila() { 
		assertTrue(pila!=null); 
		assertEquals(true, pila.isEmpty()); 
		assertEquals(tamanio, pila.size()); 
	} 
	@Test 
	public void testPop() { 
		assertTrue(top==pila.top()); 
	} 


}
