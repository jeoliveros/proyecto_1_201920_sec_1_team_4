package test.data_structures;

import model.data_structures.*;
import junit.framework.TestCase;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;



public class TestListaEnlazada<T extends Comparable<T>> extends TestCase
{
	//ESTA CLASE FUE CREADA USANDO COMO GUIA EL PROYECTO DE CUPI2 CENTRAL de pacientes



	private ListaEnlazada<T> lista;

	private int sizeL;



	//lista vacia 
	private void setupEscenario1()
	{
		lista = new ListaEnlazada<T>();
		sizeL =0;
	}


	//lista con 20 elementos
	private void setupEscenario2()
	{
		lista = new ListaEnlazada<T>();
		sizeL =20;

		for (int cont = 0; cont < sizeL; cont++) 
		{
			String a = Integer.toString(cont);

			T dato = (T)a;

			Nodo<T> aux = null;
			
			Nodo<T> nuevo= new Nodo<T>(dato, aux);

			lista.agregar((T) nuevo); ;
		}
	}

	/*
	 * 
	 */
	public void testAgregar()
	{
		setupEscenario1();
		sizeL=10;
		ArrayList<Nodo<T>> nodos = new ArrayList<Nodo<T>>( );
		Nodo<T> Node;
		Nodo<T> aux = null;

		for (int cont = 0; cont < sizeL; cont++) 
		{
			String a = Integer.toString(cont);
			T dato = (T)a;
			
			
			Node = new Nodo<T>(dato, aux);
			
			nodos.add(Node);
		}

		//agrega y verifica que se hayan agregado correctamente
		for (int cont = 0; cont < sizeL; cont++) 
		{
			String a = Integer.toString(cont);
			T dato = (T)a;

			Node = (model.data_structures.Nodo) nodos.get(cont);
			
			
			lista.agregar( (T) (Node = new Nodo<T>(dato, aux)));
			ArrayList losDatos = lista.darDatos();

			aux = (model.data_structures.Nodo)losDatos.get( 0 ); 


			//verificar que se hayan agregado 
			assertEquals("El archivo no se agrego correctamente", Node.obtenerDato(), aux.obtenerDato());


			//verifica que el numero de datos sea correcto
			assertEquals("El numero de datos no es correcto", cont+1, losDatos.size());
		}
	}


	/*
	 * 
	 */
	public void testEliminar()
	{
		setupEscenario2();

		try 
		{
			String a = Integer.toString(0);
			T dato = (T)a;

			lista.eliminar((T)dato);

			String b = Integer.toString(0);
			T dato2 = (T)b;

			Nodo nodo = (Nodo) lista.buscar((int) dato2);

			assertNull("el Nodo no esta ", nodo);
			assertEquals("El numero de pacientes no es el correcto", sizeL-1,lista.size());

			//Eliminar en el medio 
			lista.eliminar(dato2);
			nodo = (Nodo) lista.buscar(2);
			assertNull( "El nodo no debera encontrarse", nodo );
			assertEquals( "El n�mero de nodos no es el correcto", sizeL-2, lista.darDatos().size() );


			//Elinimiar en la cola 

			lista.eliminar(dato);
			nodo = (Nodo) lista.buscar(1);
			assertNull( "El nodo no deber�a encontrarse", nodo );
			assertEquals( "El n�mero de nodo no es el correcto", sizeL-3, lista.darDatos().size( ) );


			ArrayList nodoss = lista.darDatos();
			for( int cont = 0; cont < sizeL - 3; cont++ )
			{
				nodo = ( Nodo )nodoss.get( cont );
				if( cont < 4 )
				{

					assertEquals( "La eliminaci�n no se realiz� de forma correcta", cont + 1, nodo );
				}
				else if( cont > 4 )
				{
					assertEquals( "La eliminaci�n no se realiz� de forma correcta", cont + 2, nodo );
				}
			}
		} 
		catch (Exception e) {
		}
	}


	/*
	 * 
	 */
	public void testremplazar()
	{
		setupEscenario2();

		String b = Integer.toString(3);
		T dato2 = (T)b;
	
		Nodo<T> aux = null;
		
		Nodo NodoA = new Nodo<T>(dato2, aux);
		T aa = (T) NodoA;


		String a = Integer.toString(4);
		T dato = (T)a;
		Nodo NodoNu = new Nodo<T>(dato, aux);
		T bb = (T) NodoNu;

		lista.remplazar(aa, bb);

		assertNotNull("El nodo se deberia haber remplazado", NodoNu);

	}

	/*
	 * 
	 */
	public void testbuscar ()
	{
		setupEscenario2();

		Nodo nodo = (Nodo) lista.buscar(sizeL-2);
		//18
		assertNotNull("El nodo se deberia haber encontrado", nodo);
		assertEquals("El nodo no se busco de forma correcta", sizeL-2, 18);

		nodo = (Nodo) lista.buscar(sizeL-1);
		assertNotNull( "El nodo deber�a haberse encontrado", nodo );
		assertEquals( "El nodo no se busco de forma correcta", sizeL - 1, 19 );

		nodo = (Nodo) lista.buscar(1000);
		assertNull( "El nodo no deber�a haberse encontrado", nodo );

	}







}
