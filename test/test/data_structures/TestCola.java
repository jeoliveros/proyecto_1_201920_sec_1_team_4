package test.data_structures;

import static org.junit.Assert.*;
import org.junit.Test; 
import model.data_structures.Cola; 
import model.data_structures.Nodo; 

public class TestCola<T extends  Comparable<T>> 
{ 
	private Cola<T> cola; 

	private int tamanio; 



	public void setUpEscenario1() { 
		cola= new Cola<T>(); 
	} 

	@Test 
	public void testCola() { 
		setUpEscenario1(); 
		assertTrue(cola!=null); 
		assertEquals(true, cola.isEmpty()); 
		assertEquals(tamanio, cola.size()); 
	} 

	
	@Test 
	public void testObtenerPrimero() { 
		setUpEscenario1(); 
		cola.enqueue((T) new Integer(1)); 
		cola.enqueue((T) new Integer(2)); 
		assertTrue(1==(Integer) cola.DarPrimero()); 
	} 

	@Test 
	public void testEnque() { 
		setUpEscenario1(); 
		cola.enqueue((T) new Integer(1)); 
		cola.enqueue((T) new Integer(2)); 
		assertTrue( 2==cola.size()); 
		assertTrue( 1==(Integer) cola.dequeue()); 
	} 
} 
