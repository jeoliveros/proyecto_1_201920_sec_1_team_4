package view;

import model.logic.MVCModelo;
import model.logic.ParteA;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{

	}

	public void printMenu()
	{
		System.out.println("1. Cargar Datos");
		System.out.println("2. Agregar String");
		System.out.println("3. Buscar String");
		System.out.println("4. Eliminar String");
		System.out.println("5. Consultar el tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar de los viajes entre una zona de origen y una zona destino para un diÌ�a dado de la semana ");
		System.out.println("6. Consultar la informacioÌ�n de los N viajes con mayor tiempo promedio para un diÌ�a dado.");
		System.out.println("7. Comparar los tiempos promedios de los viajes para una zona dada contra cada zona X en un rango de zonas dado en ambos sentidos para un diÌ�a dado");
		System.out.println("8. Consultar los viajes entre una zona de origen y una zona destino en una franja horaria (hora inicial â€“ hora final) dada.");
		System.out.println("9.");
		System.out.println("10. Generar una graÌ�fica ASCII que muestre el tiempo promedio de los viajes entre una zona origen y una zona destino para cada hora del diÌ�a");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void printMessage(String mensaje) {

		System.out.println(mensaje);
	}		
	

	
	public void printModelo (MVCModelo modelo) 
	{
			System.out.println(modelo);
	}
	
	public void printParteA (ParteA parteA)
	{
		System.out.println(parteA);
	}
	
	/*
	public void printCargarDatos(MVCModelo modelo, String archivo)
	{
		
		System.out.println("Dato agregado");
		System.out.println("Numero actual de viajes "  + modelo.cargarDatos( archivo));	
	}
	*/
	
}
