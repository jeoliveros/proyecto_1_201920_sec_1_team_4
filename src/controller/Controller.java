package controller;

import java.util.Scanner;

import model.logic.*; 
import view.MVCView;
import model.data_structures.*;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	private ParteA parteA;
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
		parteA = new ParteA(modelo);
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:

				view.printMessage("-----\nQue trimestre desea cargar");

				dato = lector.next();

				int semestreSeleccionado = Integer.parseInt(dato);

				modelo.cargarDatos(semestreSeleccionado);


				view.printMessage("El total de datos es:"+ modelo.cargarDatos(semestreSeleccionado));

				view.printMessage("Listo");

				break;

			case 2:
				view.printMessage(" Digite el, y el mes Separados por comas sin espacios");

				dato = lector.next();

				String[] datosDelUsuario = dato.split(",");

				int mes = Integer.parseInt(datosDelUsuario[0]);
				int N = Integer.parseInt(datosDelUsuario[1]);
				

				String imprimir =parteA.funcion2A(mes, N);
				view.printMessage(imprimir);
				if (imprimir=="" );
				{

					view.printMessage("No exixtse ninguna informacion con los parametros de busqueda digitados");
				}


				break;



			case 4:
				view.printMessage(" Digite la zona min, Digite la zona max y "
						+ "Digite la zona dada Separados por comas sin espacios");

				dato = lector.next();

				String[] datosUsuario = dato.split(",");

				int min = Integer.parseInt(datosUsuario[0]);
				int max = Integer.parseInt(datosUsuario[1]);
				int Mes1 = Integer.parseInt(datosUsuario[2]);
				int zonaDada = Integer.parseInt(datosUsuario[3]);

				String imprimir1 =parteA.funcion3A(min, max, Mes1, zonaDada);
				view.printMessage(imprimir1);
				if (imprimir1=="" );
				{

					view.printMessage("No exixtse ninguna informacion con los parametros de busqueda digitados");
				}
		

				break;



			case 5 :

				view.printMessage(" Digite la zona origen, Digite la zona destino y Digite el dia de la semana Separados por comas sin espacios");

				dato = lector.next();

				String[] datosPorElUsuario = dato.split(",");

				int sourceid = Integer.parseInt(datosPorElUsuario[0]);
				int dstid = Integer.parseInt(datosPorElUsuario[1]);
				int Dia = Integer.parseInt(datosPorElUsuario[2]);

				if (modelo.tiempoPromedioDeViajeYDesviacionEstandar(sourceid, dstid, Dia).length!=0 );
				{

					//tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar

					for (int i = 0; i < modelo.tiempoPromedioDeViajeYDesviacionEstandar(sourceid, dstid, Dia).length; i++) 
					{
						view.printMessage( "El tiempo promedio del viaje "+i+ "es: "+ modelo.tiempoPromedioDeViajeYDesviacionEstandar(sourceid, dstid, Dia)[i].dargeometricMeanTravelTime()+
								" y su desviacion estandar es: "+ modelo.tiempoPromedioDeViajeYDesviacionEstandar(sourceid, dstid, Dia)[i].darstandardDeviationTravelTime());

					}

					view.printMessage("No exixtse ninguna informacion con los parametros de busqueda digitados");
				}

				if(modelo.tiempoPromedioDeViajeYDesviacionEstandar(sourceid, dstid, Dia).length== 0)
				{
					view.printMessage("No exixtse ninguna informacion con los parametros de busqueda digitados");
				}
				break;


			case 6 :
				view.printMessage(" Digite el dia de la semana y el numero de elementos que desea ver separados por ,");

				dato = lector.next();

				//Mostrar los resultados indicando para cada viaje su zona origen, zona destino, el tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar.


				String[] datosPorElUsuarioMetodo2 = dato.split(",");

				int dia = Integer.parseInt(datosPorElUsuarioMetodo2[0]);
				int numero = Integer.parseInt(datosPorElUsuarioMetodo2[1]);

				modelo.nViajesConMayorTiempoPromedioParaUnDia(dia,numero);


				for (int i = 0; i < modelo.nViajesConMayorTiempoPromedioParaUnDia(dia, numero).length ; i++) {

					view.printMessage( "Viaje numero:"+i+ "Su zona Origen es"+modelo.nViajesConMayorTiempoPromedioParaUnDia(dia, numero)[i].darsourceId()+
							" Su zona de destino es: "+ modelo.nViajesConMayorTiempoPromedioParaUnDia(dia, numero)[i].dardestinID()+
							" Su tiempo promedio de viaje es: "+ modelo.nViajesConMayorTiempoPromedioParaUnDia(dia, numero)[i].dargeometricMeanTravelTime()+
							" Su desviacion estandar es: " + modelo.nViajesConMayorTiempoPromedioParaUnDia(dia, numero)[i].darstandardDeviationTravelTime());

				}





				break;

			case 7 :

				//				Mostrar los resultados de comparacioÌ�n con cada zona X en una liÌ�nea de la siguiente forma:

				//					<tiempo promedio> de <Zona dada> a <Zona X> vs 
				//<tiempo promedio> de <Zona X> a <Zona dada>

				//					Si no hay viajes en alguno o en ambos sentidos entre la zona dada y
				//					una zona X debe reemplazarse <tiempo promedio> por "No hay viajes" en la direccioÌ�n respectiva.
				//				

				view.printMessage("Escriba la zona dada y la zona X que es contra la que se compara (la zona X tiene zona mayor y zona menor) escriba el dia ingrese los datos separados por comas sin espacios");

				dato = lector.next();

				String[] datosPorElUsuario7 = dato.split(",");

				int ZonaDada = Integer.parseInt(datosPorElUsuario7[0]);
				int ZonaXMenor = Integer.parseInt(datosPorElUsuario7[1]);
				int ZonaXMayor = Integer.parseInt(datosPorElUsuario7[2]);
				int dia7 = Integer.parseInt(datosPorElUsuario7[3]);

				modelo.ordenarAcendentemente(dia7, ZonaXMenor, ZonaXMayor, ZonaDada);

				String Mostar;
				viajeUberDia viajeDireccion1 = null;
				viajeUberDia viajeDireccion2 = null;


				for (int i = 0; i < modelo.ordenarAcendentemente(dia7, ZonaXMenor, ZonaXMayor, ZonaDada).length; i++) 
				{
					viajeUberDia viajeAMostar = modelo.ordenarAcendentemente(dia7, ZonaXMenor, ZonaXMayor, ZonaDada)[i];


					if (
							(viajeAMostar.darsourceId()== ZonaDada && (viajeAMostar.dardestinID()== ZonaXMayor| viajeAMostar.dardestinID() < ZonaXMayor| viajeAMostar.dardestinID()== ZonaXMenor| viajeAMostar.dardestinID()> ZonaXMenor))
							)
					{
						viajeDireccion1= viajeAMostar;
					}

					for (int i2 = 0; i2 < modelo.ordenarAcendentemente(dia7, ZonaXMenor, ZonaXMayor, ZonaDada).length; i2++) 
					{
						viajeUberDia viajeAMostar2 = modelo.ordenarAcendentemente(dia7, ZonaXMenor, ZonaXMayor, ZonaDada)[i2];


						if 
						(
								((viajeAMostar2.darsourceId()== ZonaXMayor| viajeAMostar2.darsourceId()== ZonaXMenor |viajeAMostar2.darsourceId()< ZonaXMayor | viajeAMostar2.darsourceId()> ZonaXMenor) && viajeAMostar2.dardestinID()==ZonaDada)
								)
						{
							viajeDireccion2= viajeAMostar2;
						}


						view.printMessage("<"+viajeAMostar.darmeanTravelTime()+"> de <"+viajeAMostar.darsourceId()+"> a <"+viajeAMostar.dardestinID()+
								"> VS <"+
								viajeAMostar2.darmeanTravelTime()+"> de <"+viajeAMostar2.darsourceId()+"> a <"+viajeAMostar2.dardestinID()
								);
					}
				}




				break;

			case 8:

				/*
				 * Mostrar los viajes indicando el tiempo promedio de viaje y 
				 * su desviacioÌ�n estaÌ�ndar para cada hora entera iniciando en la hora inicial y terminando en la hora final.
				 */

				view.printMessage(" Digite la zona origen, Digite la zona destino, Digite la franja horaria (hora inicial y hora final) los 4 datos que se piden ingreselos Separados por comas sin espacios");

				dato = lector.next();

				String[] datosPorElUsuario8 = dato.split(",");

				int sourceid8 = Integer.parseInt(datosPorElUsuario8[0]);
				int dstid8 = Integer.parseInt(datosPorElUsuario8[1]);
				int HoraIni = Integer.parseInt(datosPorElUsuario8[2]);
				int Horafinal = Integer.parseInt(datosPorElUsuario8[3]);


				Pila<viajeUberHora> pilaARetornar = modelo.consultarViajesZonaOrigenZonaDestinoFranjaHoraria(sourceid8, dstid8, HoraIni, Horafinal);  

				viajeUberHora[] mostar = new viajeUberHora[pilaARetornar.size()];

				for (int horas = HoraIni; horas < Horafinal+1 ; horas++) {

					for (int i = 0; i < mostar.length; i++) {

						if(mostar[i].darHod()== horas){

							view.printMessage("El tiempo promedio es: "+ mostar[i].darmeanTravelTime()+ "La desviacion estanadar es: " + mostar[i].darstandardDeviationTravelTime()+ "para la hora" + horas );

						}


					}
				}
				break;

			case 9:
				view.printMessage(" Digite el mes, y la cantidad de viajes que quiere ver Separados por comas sin espacios");

				dato = lector.next();

				String[] datosUsuario3 = dato.split(",");


				int Mes3 = Integer.parseInt(datosUsuario3[0]);
				int N1 = Integer.parseInt(datosUsuario3[1]);
				String imprimir3 =parteA.funcion2A(Mes3, N1);
				view.printMessage(imprimir3);
				if (imprimir3=="" );
				{

					view.printMessage("No exixtse ninguna informacion con los parametros de busqueda digitados");
				}



				break;

			case 10:

				view.printMessage(" Digite la zona origen, Digite la zona destino y el trimestre Separados por comas sin espacios");

				dato = lector.next();

				String[] datosPorElUsuario10 = dato.split(",");

				int sourceid10 = Integer.parseInt(datosPorElUsuario10[0]);
				int dstid10 = Integer.parseInt(datosPorElUsuario10[1]);
				int semestreSeleccionado10 = Integer.parseInt(datosPorElUsuario10[2]);
				modelo.cargarDatos(semestreSeleccionado10);
				




				view.printMessage("AproximacioÌ�n en minutos de viajes entre zona origen y zona destino.");
				view.printMessage("Trimestre "+ semestreSeleccionado10 +"del 2018 detallado por cada hora del diÌ�a");
				view.printMessage("Zona Origen"+sourceid10);
				view.printMessage("Zona Destino"+dstid10);


				view.printMessage("Aproximación en minutos de viajes entre zona origen y zona destino.");
				view.printMessage("Trimestre "+ semestreSeleccionado10 +" del 2018 detallado por cada hora del día");
				view.printMessage("Zona Origen "+sourceid10);
				view.printMessage("Zona Destino "+dstid10);


				modelo.consultarViajesZonaOrigenZonaDestinoPUNTO3C(sourceid10, dstid10);

				viajeUberHora[] aux = new viajeUberHora[modelo.consultarViajesZonaOrigenZonaDestinoPUNTO3C(sourceid10, dstid10).size()];

				
				view.printMessage(
						"Hora | # de minutos\n"+ 
								"00   |  " + modelo.DarNumeroAstericos(0, sourceid10, dstid10) + " \n"+ 
								"01   |  " + modelo.DarNumeroAstericos(1, sourceid10, dstid10) + " \n" + 
								"02   |  " + modelo.DarNumeroAstericos(2, sourceid10, dstid10) + " \n"+
								"03   |  " + modelo.DarNumeroAstericos(3, sourceid10, dstid10) + " \n"+ 
								"04   |  " + modelo.DarNumeroAstericos(4, sourceid10, dstid10) + " \n"+ 
								"05   |  " + modelo.DarNumeroAstericos(5, sourceid10, dstid10) + " \n"+
								"06   |  " + modelo.DarNumeroAstericos(6, sourceid10, dstid10) + " \n"+ 
								"07   |  " + modelo.DarNumeroAstericos(7, sourceid10, dstid10) + " \n" + 
								"08   |  " + modelo.DarNumeroAstericos(8, sourceid10, dstid10) + " \n" +
								"09   |  " + modelo.DarNumeroAstericos(9, sourceid10, dstid10) + " \n"+ 
								"10   |  " + modelo.DarNumeroAstericos(10, sourceid10, dstid10) + " \n"+ 
								"11   |  " + modelo.DarNumeroAstericos(11, sourceid10, dstid10) + " \n"+ 
								"12   |  " + modelo.DarNumeroAstericos(12, sourceid10, dstid10) + " \n"+ 
								"13   |  " + modelo.DarNumeroAstericos(13, sourceid10, dstid10) + " \n"+ 
								"14   |  " + modelo.DarNumeroAstericos(14, sourceid10, dstid10) + " \n"+ 
								"15   |  " + modelo.DarNumeroAstericos(15, sourceid10, dstid10) + " \n"+ 
								"16   |  " + modelo.DarNumeroAstericos(16, sourceid10, dstid10) + " \n"+ 
								"17   |  " + modelo.DarNumeroAstericos(17, sourceid10, dstid10) + " \n"+ 
								"18   |  " + modelo.DarNumeroAstericos(18, sourceid10, dstid10) + " \n"+ 
								"19   |  " + modelo.DarNumeroAstericos(19, sourceid10, dstid10) + " \n"+ 
								"20   |  " + modelo.DarNumeroAstericos(20, sourceid10, dstid10) + " \n"+ 
								"21   |  " + modelo.DarNumeroAstericos(21, sourceid10, dstid10) + " \n"+ 
								"22   |  " + modelo.DarNumeroAstericos(22, sourceid10, dstid10) + " \n"+  
								"23   |  " + modelo.DarNumeroAstericos(23, sourceid10, dstid10) + " \n"

						);


				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
