package model.data_structures;

public interface IListaEnlazada <T extends Comparable<T>>
{

	T darPrimero();
	
	T darSiguiente() ;
	
	int size ();

	void agregar(T dato);

	T eliminar(T dato);

	void remplazar(T dato, T dato2);

	T buscar (int pos);
	

	

}
