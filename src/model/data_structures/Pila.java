package model.data_structures;

public class Pila<T extends Comparable<T>> implements IPila<T>
{

	
	
	private Nodo<T> top;
	
	private int tamanio;
	
	
	
	
	public Pila ()
	{
		top=null;
		tamanio=0;
	}
	
	
	
	public boolean isEmpty()
	{
		
		if (tamanio==0)
		{
			return true;
		}
		else 
		{
			return false;
		}
		
		
	}
	
	
	public int size()
	{
		return tamanio;
	}
	
	
	
	public T pop() 
	{
		T resp = null;
		
		if(isEmpty()==true )
		{
			resp=null;
		}
		else
		{
			T dato =  top.obtenerDato();
			
			Nodo<T> aux = top.darSiguiente();
			
			top=null;
			top=aux;
			tamanio--;	
			resp = dato;
		}
		
		return resp;
	}

	
	public T darElemento()
	{
		return top.obtenerDato();
	}
	
	public void push(T elemento)
	{
		Nodo<T> nodo = new Nodo<T>(elemento, top);
		top = nodo;
		tamanio++;
		
		
	}
	
	
	
	public T top()
	{
		T resp = null;
		
		if(isEmpty()== true)
		{
			return resp;
		}
		else
		{
			return top.obtenerDato();
		}
	}

	

}

