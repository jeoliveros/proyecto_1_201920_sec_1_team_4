package model.data_structures;

public interface IPila <T extends Comparable<T>>
{

	boolean isEmpty();
	
	int size();
	
	T pop();
	
	void push (T elemento);
	
	T top();
	
	T darElemento();
	
	
}
