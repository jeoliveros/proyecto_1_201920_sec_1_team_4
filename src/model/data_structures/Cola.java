package model.data_structures;

public class Cola <T extends Comparable<T>> implements ICola<T>
{
	
	
	private Nodo<T> primero;
	
	private Nodo<T> ultimo;
	
	private int tamanio;
	
	
	
	
	public Cola()
	{
		primero = null;
		ultimo= null;
		tamanio= 0;
		
		
	}
	
	
	
	public boolean isEmpty()
	{
		
		if (tamanio==0)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	
	public int size()
	{
		return tamanio;
	}
	
	
	
	
	public T DarPrimero()
	{
		if (isEmpty()==true)
		{
			return null;
		}
		else 
		{
			return primero.obtenerDato();
		}
	}
	
	
	public T darUltimo()
	{
		if (isEmpty()==true)
		{
			return null;
		}
		else if(size()==1)
		{
			return primero.obtenerDato();
		}
		else 
		{
			return ultimo.obtenerDato();
		}
	}
	
	
	public T enqueue(T elemento)
	{
		Nodo<T> aAnadir = new Nodo<T>(elemento, null);
		
		if (isEmpty()==true)
		{
			primero= aAnadir;
			ultimo = aAnadir;
		}
		else
		{
			if(size()==1)
			{
				ultimo= aAnadir;
				primero.cambiarSiguiente(ultimo);
			}
			else
			{
				ultimo.cambiarSiguiente(aAnadir);
				ultimo= aAnadir;	
			}
		}
		tamanio++;
		
		return aAnadir.obtenerDato();
	}
	
	
	
	
	
	public T dequeue()
	{
		T resp = null;
		
		if( isEmpty()== true)
		{
			resp = null;
		}
		else
		{
			
			resp = primero.obtenerDato();
					
					
			Nodo<T> aux = (Nodo<T>) primero.obtenerDato();
			primero= null;
			
			primero = aux;
			tamanio--;
			
			if (isEmpty()==true)
			{
				ultimo = null;
			}
		}
		return resp;
	}
	
	
	
	
	
	
	
	
	
	
}
