package model.data_structures;

public interface ICola <T extends Comparable<T>> 
{

	boolean isEmpty();
	
	int size();
	
	T DarPrimero();

	T darUltimo();
	
	T enqueue(T elemento);
	
	
	T dequeue();
	
}
