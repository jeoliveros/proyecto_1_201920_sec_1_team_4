package model.logic;

public class viajeUberMes implements Comparable<viajeUberMes>
{
private static int sourceId ;
	
	private static int destinID;
	
	private static int mes;
	
	private  static double meanTravelTime;
	
	private static double standardDeviationTravelTime;
	
	private static double geometricMeanTravelTime;
	
	private static double geometricStandardDeviationTravelTime;
	
	public viajeUberMes(int pSource, int pdestinID, int  pmes, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		mes = pmes;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public static int darsourceId() 
	{
		return sourceId;
	}
	
	
	public static int dardestinID() 
	{
		return destinID;
	}
	
	
	public static int darMes() 
	{
		return mes;
	}
	
	
	public static double darmeanTravelTime() 
	{
		return meanTravelTime;
	}
	
	
	public static double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public static double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public static double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}


	@Override
	public int compareTo(viajeUberMes o) {
		
		return 0;
	}

}
