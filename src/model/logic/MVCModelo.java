package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import model.data_structures.*;
import model.logic.*;
import view.MVCView;;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo <T extends Comparable<T>>
{

//	public final static String MES1 = "./data/Mes1.txt";
//	public final static String HORA1 = "./data/Hora1.txt";
//	public final static String SEMANA1 = "./data/Semana1.txt";
	
	public final static String MES1 = "./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv";
	public final static String HORA1 = "./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv";
	public final static String SEMANA1 = "./data/bogota-cadastral-2018-1-All-WeeklyAggregate.csv";

	public final static String MES2 = "./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv";
	public final static String HORA2 = "./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";
	public final static String SEMANA2 = "./data/bogota-cadastral-2018-2-All-WeeklyAggregate.csv";

	public final static String MES3 = "./data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv";
	public final static String HORA3 = "./data/bogota-cadastral-2018-3-All-HourlyAggregate.csv";
	public final static String SEMANA3 = "./data/bogota-cadastral-2018-3-All-WeeklyAggregate.csv";

	public final static String MES4 = "./data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv";
	public final static String HORA4 = "./data/bogota-cadastral-2018-4-All-HourlyAggregate.csv";
	public final static String SEMANA4 = "./data/bogota-cadastral-2018-4-All-WeeklyAggregate.csv";






	/**
	 * Atributos del modelo del mundo
	 */


	//private Pila datosEnPila;

	//private ICola datosEnCola;

	private IListaEnlazada<viajeUberMes> datosListaM;
	private IListaEnlazada<viajeUberHora> datosListaH;
	private IListaEnlazada<viajeUberDia> datosListaS;

	private IPila<viajeUberDia> pila;
	//private MVCView view;


	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{


		//view = view = new MVCView();

		pila = new Pila<viajeUberDia>();

		//datosEnCola = new Cola();

		datosListaM = new ListaEnlazada<viajeUberMes>() ;
		datosListaH = new ListaEnlazada<viajeUberHora>() ;
		datosListaS = new ListaEnlazada<viajeUberDia>() ;



	}


	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */

	public int darTamanoListaM()
	{
		return datosListaM.size();
	}

	public int darTamanoListaS()
	{
		return datosListaS.size();
	}

	public int darTamanoListaH()
	{
		return datosListaH.size();
	}



	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */


	public void agregarEnLista(viajeUberMes dato)
	{
		datosListaM.agregar(dato);
	}

	public void agregarEnLista(viajeUberDia dato)
	{
		datosListaS.agregar(dato);
	}

	public void agregarEnLista(viajeUberHora dato)
	{
		datosListaH.agregar(dato);
	}


	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */



	public viajeUberDia eliminarEnListaD (viajeUberDia dato)
	{
		return (viajeUberDia) datosListaS.eliminar(dato);
	}

	public viajeUberMes eliminarEnListaM (viajeUberMes dato)
	{
		return (viajeUberMes) datosListaM.eliminar(dato);
	}

	public viajeUberHora eliminarEnListaH (viajeUberHora dato)
	{
		return (viajeUberHora) datosListaH.eliminar(dato);
	}


	public IListaEnlazada getDatosMesLista() {
		return datosListaM;
	}



	public String cargarDatos(int TrimestreSeleccionado) 
	{

		int lienasMes= 0;
		int lineasSemana= 0;
		int lineasHora = 0;



		if(TrimestreSeleccionado==1)
		{

			lineasHora = CargarHora(HORA1);

			lineasSemana = CargarSemana(SEMANA1) ;

			lienasMes = CargarMes(MES1);

		}
		else if (TrimestreSeleccionado==2)
		{
			lineasHora = CargarHora(HORA2);
			lineasSemana = CargarSemana(SEMANA2);
			lienasMes = CargarMes(MES2);
		}
		else if (TrimestreSeleccionado==3)
		{
			lineasHora = CargarHora(HORA3);
			lineasSemana = CargarSemana(SEMANA3);
			lienasMes = CargarMes(MES3);
		}
		else if (TrimestreSeleccionado==4)
		{
			lineasHora = CargarHora(HORA4);
			lineasSemana = CargarSemana(SEMANA4);
			lienasMes = CargarMes(MES4);
		}
		else {

		}

		return ("Los viajes del mes son: "+ lienasMes + ". Los viajes de la semana son: " + lineasSemana+ ". Los viajes de hora son: " + lineasHora );
	}





	public int CargarMes( String archivomes) 
	{


		File archivo = new File(archivomes);

		FileReader lector;
		int totalViajes=0;

		try {


			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();
			linea= bf.readLine();


			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);



				viajeUberMes viaje = new viajeUberMes(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				datosListaM.agregar((viajeUberMes) viaje);


				linea = bf.readLine();
				totalViajes++;

			}


		}
		catch (Exception e) {
			e.printStackTrace();

		}

		return totalViajes;

	}


	public int CargarHora(String archivohora )
	{

		int totalViajes=0;


		File archivo = new File(archivohora);

		FileReader lector;

		try {
			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);
			String linea = bf.readLine();
			linea= bf.readLine();


			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);

				viajeUberHora viaje = new viajeUberHora(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				datosListaH.agregar((viajeUberHora) viaje);

				linea = bf.readLine();
				totalViajes++;
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}

		return totalViajes;

	}


	public int CargarSemana(String archivosemana )
	{
		int totalViajes=0;


		
		File archivo = new File(archivosemana);

		FileReader lector;

		try {
			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);
			String linea = bf.readLine();
			linea= bf.readLine();

			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);

				viajeUberDia viaje = new viajeUberDia(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				datosListaS.agregar((viajeUberDia) viaje);
				pila.push(viaje);

				linea = bf.readLine();
				totalViajes++;
				
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		return totalViajes;

	}




	/**
	 * PARTE B PUNTO 1
	 * 
	 * Consultar el tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar de los viajes entre una zona de origen 
	 * y una zona destino para un diÌ�a dado de la semana. 
	 * Reportar el caso especial en que No exista informacioÌ�n al respecto.  dow
	 * @param pZonaOrigen
	 * @param pZonaDestino
	 * @param pDiaSemana
	 * @return
	 */
	public viajeUberDia[] tiempoPromedioDeViajeYDesviacionEstandar (int pZonaOrigen, int pZonaDestino, int pDiaSemana)
	{
		
		Pila<viajeUberDia> pilaConDatos= null;
		
		Pila<viajeUberDia> pilaAUsar = (Pila<viajeUberDia>) pila;
		

		viajeUberDia topViaje = (viajeUberDia) pilaAUsar.top();

		while(topViaje!=null){

			viajeUberDia verificar = (viajeUberDia) topViaje ;

			
			
			if (verificar.darsourceId() == pZonaOrigen && verificar.dardestinID()==pZonaDestino && verificar.dardia()==pDiaSemana )	 {
				pilaConDatos.push(verificar); 
				
				
			}
			pilaAUsar.pop();
		}
		
		viajeUberDia[] listaConViajesResp = new viajeUberDia[pilaConDatos.size()];
		

		return listaConViajesResp;
	}



	/*
	 *  PARTE B PUNTO 2
	 *  
	 *  Consultar la informacioÌ�n de los N viajes con mayor tiempo promedio para un diÌ�a dado. 
	 *  La informacioÌ�n debe mostrarse ordenada de mayor a menor por el tiempo promedio de los viajes. 
	 *  Mostrar los resultados indicando para cada viaje su zona origen, zona destino, el tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar.
	 * @param dia
	 * @return
	 */
	public viajeUberDia[] nViajesConMayorTiempoPromedioParaUnDia(int dia , int pNviajes)
	{

		viajeUberDia[] listaConViajes = new viajeUberDia[ordenarDeMayorAMenor(dia).length];

		viajeUberDia[] listaConViajesResp = null;

		for (int i = 0; i < listaConViajes.length && i<pNviajes; i++) {

			listaConViajesResp[i] = listaConViajes[i];
		}


		return listaConViajesResp;
	}







	/**
	 *  PARTE B PUNTO 2
	 *  
	 * @param dia
	 * @return
	 */
	public Pila<viajeUberDia> buscaLosViajesEnUnDia (int dia )
	{
		Pila<viajeUberDia> pilaARetornar = new Pila<viajeUberDia>() ;

		Pila<viajeUberDia> pilaAUsar = (Pila<viajeUberDia>) pila;

		viajeUberDia topViaje = (viajeUberDia) pilaAUsar.top();


		while(topViaje!=null){
			viajeUberDia verificar = (viajeUberDia) topViaje ;

			if (verificar.dardia()==dia){
				pilaARetornar.push(verificar);
			}
			pilaAUsar.pop();
		}
		return pilaARetornar;
	}


	/**
	 *  PARTE B PUNTO 2
	 * Ordenar los n viajes de mayor a menor por el tiempo promedio de los viajes 
	 */
	public viajeUberDia[] ordenarDeMayorAMenor (int dia)
	{

		viajeUberDia[] listaConDatos = new viajeUberDia[buscaLosViajesEnUnDia(dia).size()];

		

		return mergeSort(0, listaConDatos.length-1, dia);
	}


	/**
	 *  PARTE B PUNTO 2
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo MergeSort.
	 * @param plow
	 * @param phigh
	 * @param hora
	 * @return
	 */
	public viajeUberDia[] mergeSort( double plow, double phigh, int dia)
	{

		viajeUberDia[] aux = new viajeUberDia[buscaLosViajesEnUnDia(dia).size()];

		if(plow >= phigh)
		{
			return aux;
		}

		double  middle = (plow+ phigh)/2;

		mergeSort( plow, middle,dia);

		mergeSort( middle+1, phigh, dia);

		merge (  plow,middle, phigh, dia);

		return aux;


	}



	/**
	 *  PARTE B PUNTO 2
	 * @param Plow
	 * @param Pmiddle
	 * @param Phigh
	 * @param dia
	 */
	private void merge ( double Plow, double Pmiddle, double Phigh, int dia )
	{
		viajeUberDia[] listaAOrdenar = new viajeUberDia[buscaLosViajesEnUnDia(dia).size()];

		viajeUberDia[] aux = new viajeUberDia[buscaLosViajesEnUnDia(dia).size()];

		double i = Plow;
		double j = Pmiddle+1;
		double k = Plow;

		while ((i <= Pmiddle) &&  (j <= Phigh)){   
			if (listaAOrdenar[(int) i].compareTo(listaAOrdenar[(int) j])==(-1|0)){

				listaAOrdenar[(int) k]= aux[(int) i];
				i++;
			}

			else{

				listaAOrdenar[(int) k]= aux[(int) j];
				j++;
			}
		}

		k++;

		while(i<= Pmiddle) {
			listaAOrdenar[(int) k]= aux[(int) i];
			k++;
			i++;
		}

		while(j<= Phigh) {
			listaAOrdenar[(int) k]= aux[(int) j];
			k++;
			j++;
		}
	}









	/**
	 * PARTE B PUNTO 3
	 * 
	 *Comparar los tiempos promedios de los viajes para una zona dada contra cada zona X en un rango de zonas dado [Zona menor, Zona Mayor] 
	 * en ambos sentidos (zona dada â€“ zona X vs. zona X â€“ zona dada) para un diÌ�a dado
	 *Los resultados deben estar ordenados ascendentemente por el identificador de la zona X en el rango dado.
	 * 
	 * 
	 * ej me dan 8 ---> 13, 12 , 11, 10, 9
	 * 
	 * y de -->> 9,10,11,12,13 --> 8
	 * 
	 * @param dia
	 */
	public Pila<viajeUberDia> ListaConLosViajesDeCadaZona (int dia, int ZonaXMenor , int ZonaXMayor, int ZonaDada  )
	{

//Pila a retornar
		Pila<viajeUberDia> pilaARetornar = new Pila<viajeUberDia>() ;
//Pila que se va a usar para hacer la comparacion
		Pila<viajeUberDia> pilaAUsar = (Pila<viajeUberDia>) pila;
// se usa una variable para indentificar el primer elemento de la pila
		viajeUberDia topViaje = (viajeUberDia) pilaAUsar.top();

//Mientras el primero  en salir no sea nulo
		while(topViaje!=null )
		{
//El elemento viajeUberDia topViaje  lo convertimos en comp			
			viajeUberDia comp = (viajeUberDia) topViaje ;
//si cumple que es el mismo dia que el dado en parametro continuamos
			if (comp.dardia()==dia)
			{
//Si comp cumple con la zona dada por parametro, zonaXMayor y zonaXMenor
				if(
					(comp.darsourceId()== ZonaDada && (comp.dardestinID()== ZonaXMayor| comp.dardestinID() < ZonaXMayor| comp.dardestinID()== ZonaXMenor| comp.dardestinID()> ZonaXMenor))|  
					((comp.darsourceId()== ZonaXMayor| comp.darsourceId()== ZonaXMenor |comp.darsourceId()< ZonaXMayor | comp.darsourceId()> ZonaXMenor) && comp.dardestinID()==ZonaDada)
				  )
				{
					pilaARetornar.push(comp);	
				}
				
			}
			pilaAUsar.pop();
		}
		return pilaARetornar;

	}


	
	
	/**
	 * PARTE B PUNTO 3
	 * @param dia
	 * @param ZonaXMenor
	 * @param ZonaXMayor
	 * @param ZonaDada
	 * @return
	 */
	public viajeUberDia[] ordenarAcendentemente (int dia, int ZonaXMenor , int ZonaXMayor, int ZonaDada )
	{
		
		viajeUberDia[] listaConDatos = new viajeUberDia[shellSort(dia, ZonaXMenor, ZonaXMayor, ZonaDada).length];
		
		
		return listaConDatos;
	}


	/**
	 * PARTE B punto 3 Metodo que ordena 
	 * @param dia
	 * @param ZonaXMenor
	 * @param ZonaXMayor
	 * @param ZonaDada
	 * @return
	 */
	public viajeUberDia[] shellSort(int dia, int ZonaXMenor , int ZonaXMayor, int ZonaDada)
	{
		
		viajeUberDia[] listaConViajes = new viajeUberDia[ListaConLosViajesDeCadaZona(dia, ZonaXMenor, ZonaXMayor, ZonaDada).size()];
		
		int tamanoArreglo;
		int b;
		viajeUberDia c;
		
		boolean paso;
		
		tamanoArreglo= listaConViajes.length;
		
		while(tamanoArreglo>0)
		{
			tamanoArreglo = tamanoArreglo/2;
			
			paso =true;
			
			while(paso==true)
			{
				paso= false;
				b = 0;
				
				while ((b + tamanoArreglo) <= listaConViajes.length-1)
					
				{
					if ( listaConViajes[b].compareTo(listaConViajes[b + tamanoArreglo])== -1 )
					{
						c = listaConViajes[b];
						listaConViajes[b] = listaConViajes[b+tamanoArreglo];
						listaConViajes[b+tamanoArreglo] = c;
						paso = true;
					}
					b = b +1;
				}
				
			}
		}
		return listaConViajes;
	}
	


	
	




	/*
	 * 
	 * PARTE C PUNTO 1
	 * 
	 * Consultar los viajes entre una zona de origen y una zona destino en una franja horaria (hora inicial – hora final) dada.
	 *  La franja horaria se define con horas enteras. Mostrar los viajes indicando el tiempo promedio de viaje 
	 *  y su desviación estándar para cada hora entera iniciando en la hora inicial y terminando en la hora final.
	 */
	public Pila<viajeUberHora> consultarViajesZonaOrigenZonaDestinoFranjaHoraria(int ZonaOrigen, int zonadestino, int HoraIni, int Horafinal)
	{
		Pila<viajeUberHora> pilaARetornar = new Pila<viajeUberHora>() ;

		Pila<viajeUberHora> pilaAUsar = ((Pila<viajeUberHora>) datosListaH);

		viajeUberHora topViaje = pilaAUsar.top() ;


		while(topViaje!=null ){
			viajeUberHora verificar = (viajeUberHora) topViaje ;

			if (verificar.darHod()> HoraIni | verificar.darHod()==HoraIni && verificar.darHod()< Horafinal | verificar.darHod()==Horafinal && 
					ZonaOrigen == verificar.darsourceId()&& zonadestino== verificar.dardestinID()){

				pilaARetornar.push(verificar);
			}
			pilaAUsar.pop();
		}
		return pilaARetornar;


	}









	/**
	 * PARTE C PUNTO 3
	 * 
	 * @param ZonaOrigen
	 * @param zonadestino
	 * @return
	 */
	public Pila<viajeUberHora> consultarViajesZonaOrigenZonaDestinoPUNTO3C (int ZonaOrigen, int zonadestino)
	{
		Pila<viajeUberHora> pilaARetornar = new Pila<viajeUberHora>() ;

		Pila<viajeUberHora> pilaAUsar = (Pila<viajeUberHora>) datosListaH;

		viajeUberHora topViaje = pilaAUsar.top() ;


		while(topViaje!=null ){
			viajeUberHora verificar = (viajeUberHora) topViaje ;

			if (ZonaOrigen == verificar.darsourceId()&& zonadestino== verificar.dardestinID() ){

				pilaARetornar.push(verificar);
			}
			pilaAUsar.pop();
		}
		return pilaARetornar;


	}



	
	/**
	 * PARTE C PUNTO 3
	 * 
	 * @param hora
	 * @param sourceid10
	 * @param dstid10
	 * @return
	 */
	public String DarNumeroAstericos ( int hora, int sourceid10, int dstid10)
	{
		//un * corresponde a un minuto 
		//Un tiempo promedio se aproxima a los minutos más cercanos, ejemplo: 2420 segundos se aproxima a 40 minutos, 
		//2450 segundos se aproxima a 41 minutos y 2430 se aproxima a 41 minutos

		viajeUberHora[] aux = new viajeUberHora[consultarViajesZonaOrigenZonaDestinoPUNTO3C(sourceid10, dstid10).size()];

		viajeUberHora aRetornar= null;

		String resp = "";

		for (int i = 0; i < aux.length; i++) 
		{
			aRetornar = aux[i];

			if(aRetornar.darHod()== hora && aRetornar!=null )
			{
				double seg = (aRetornar.darmeanTravelTime()/60);
				int enMinutos = (int) Math.round(seg);

				if(enMinutos!=0){
					for (int j = 0; j < enMinutos; j++) {
						resp = resp+"*";
					}
				}
				else{
					resp="hora sin viajes";
				}
			}
			else {
				resp="hora sin viajes";	
			}	
		}
		return resp;
	}















}
