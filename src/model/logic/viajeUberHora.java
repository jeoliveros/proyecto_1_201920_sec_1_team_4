package model.logic;

public class viajeUberHora implements Comparable<viajeUberHora>
{

private static int sourceId ;
	
	private static int destinID;
	
	private static int Hod;
	
	private  static double meanTravelTime;
	
	private static double standardDeviationTravelTime;
	
	private static double geometricMeanTravelTime;
	
	private static double geometricStandardDeviationTravelTime;
	
	public viajeUberHora(int pSource, int pdestinID, int  pHod, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		Hod = pHod;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public static int darsourceId() 
	{
		return sourceId;
	}
	
	
	public static int dardestinID() 
	{
		return destinID;
	}
	
	
	public static int darHod() 
	{
		return Hod;
	}
	
	
	public static double darmeanTravelTime() 
	{
		return meanTravelTime;
	}
	
	
	public static double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public static double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public static double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}


	@Override
	public int compareTo(viajeUberHora o) 
	{
		
		return 0;
	}
	
}
