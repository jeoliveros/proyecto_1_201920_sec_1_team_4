package model.logic;

import model.data_structures.*;

public class ParteA {

	/**darViajesporMes 
	 * 
	 */

	private MVCModelo modelo;

	public ParteA(MVCModelo pModelo) {

		modelo = pModelo;

	}
	/**
	 * Este metodo recorre una lista con todos los viajes que contienen el atributo mes y ponemos en una nueva lista que se va a 
	 * retornar todos los viajes en una lista viajeMes  que contenga el mes dado por parametro
	 * @param mes
	 * @return viajesMes que contiene todo los viajes que son en un mes predeterminado
	 */
	public IListaEnlazada<viajeUberMes> darViajesPorMes(int mes){
		//Se crea lista nueva
		IListaEnlazada<viajeUberMes> viajeMes = new ListaEnlazada<viajeUberMes>();
		//se guardar todos los 	que contengan mes en una lista 
		IListaEnlazada<viajeUberMes> aux = modelo.getDatosMesLista();
		// Se recorre la lista aux que contiene todos los datos con mes
		for (int i = 0; i < aux.size(); i++) {

			viajeUberMes actual = aux.buscar(i);
			//Verficamos que el mes de el objeto que se esta evaluando es igual de parametro
			if(actual.darMes()== mes) {
				//Si es igual los agregamos a la list viajeMes
				viajeMes.agregar(actual);
			}
		}

		return viajeMes;
	}

	/**
	 * Este metodo se encarga de crear y devolver una lista que contenga todos los viajes que cumplan el mismo lugar de partida, de llegada y
	 * del mes dado por parametro en el metodo anterior
	 * @param inicio
	 * @param destino
	 * @param listaViajes
	 * @return una lista que cumple con los parametro antes establecidos
	 */

	public IListaEnlazada<viajeUberMes> darViajesEntre(int inicio,int destino,IListaEnlazada<viajeUberMes> listaViajes) {
		/*Creamos una lista de viajeUberMes que va a guardar todos los viajes que cumplan con un inicio, destino y una lista que contiene
		 * todos los viajes con un mes dado por parametro, esto se hizo en el anterior metodo.
		 */	
		IListaEnlazada<viajeUberMes> viajes = new ListaEnlazada<viajeUberMes>();
		//Se recorre la lista que contiene los viajes por un mes dado.
		for (int i = 0; i < listaViajes.size(); i++) {
			// Si el objeto que se saca tiene el mismo inicio y destino dado por parametro se agrega a la nueva lista
			if(listaViajes.buscar(i).dardestinID()== destino && listaViajes.buscar(i).darsourceId()== inicio) {
				viajes.agregar(listaViajes.buscar(i));
			}
		}

		return viajes;
	}
	/**
	 * Se recorre la lista establecida de obtener los viajes que cumplen con un lugar de salida y un lugar de llegada y un mes dado 
	 * y los imprima en un string
	 * @param listaViajes
	 * @return retorna en un string cada uno de los datos de la lista listaViajes
	 */
	public String respuesta1A(IListaEnlazada<viajeUberMes> listaViajes) {
		String resp="";

		/*
		 * 1. recorrer la lista
		 * 	1.1 recuperar los datos del viaje
		 * 	1.2 meter los datos relevantes al string resp
		 */
		for (int i = 0; i < listaViajes.size(); i++) {
			viajeUberMes actual = listaViajes.buscar(i); //1.1
			resp+="El viaje uber numero " + (i+1) + "tiene un tiempo promedio "+ actual.darmeanTravelTime() +
					"y su desviacion es " + actual.darstandardDeviationTravelTime()+ "\n";
		}

		return resp;
	}

	/**
	 * funcion1A se encarga de unir todos los metodos antes dichos para crear el metodo "madre"  que cumple todos
	 * los requisitos del punto 1A.
	 * @param inicio
	 * @param destino
	 * @param mes
	 * @return nNos retorna un String con la informacion de todos los viajes que coumplen con los requisitos
	 */
	public String funcion1A(int inicio,int destino,int mes){
		String viajesFinales = "";

		/*
		 * 1. Filtrar por mes
		 * 2. Filtrar por inicio y destino (recibe lo que saque 1)
		 * 3. toString (recibe lo que saque 2)
		 * 4. return 3
		 * 
		 */
		IListaEnlazada<viajeUberMes> viajesMes=darViajesPorMes(mes);
		IListaEnlazada<viajeUberMes> viajesConInDesMes=darViajesEntre(inicio, destino, viajesMes);
		viajesFinales= respuesta1A(viajesConInDesMes);		
		return viajesFinales;
	}
	/**Ojo este ordenamiento es de mayor a menor
	 * 	Este metodo se encarga de odernar por tiempo una lista dada por parametro que contiene todos los viajes de un
	 * mes dado por parametro en un metodo anterior
	 * @param listaViajesMes
	 * @return una lista de viajes ordenada por un mes dado por parametro
	 */
	public IListaEnlazada<viajeUberMes> ordenarPorTiempo(IListaEnlazada<viajeUberMes> listaViajesMes){
		for (int i = 0; i < listaViajesMes.size(); i++) {
			for (int j = 1; j < listaViajesMes.size(); j++) {
				//Se crea una variable que contenga al anterior objeto a donde estamos parados en la actualidad
				int anterior=j-1;
				//Se compara y ordena (descendentemente) por el tiempo promedio de viaje y su desviacion estandar el anterior y el actual
				if(listaViajesMes.buscar(anterior).darmeanTravelTime()< listaViajesMes.buscar(j).darmeanTravelTime()) {
					//Se guarda en una variable el objeto anterior al actual
					viajeUberMes aux = listaViajesMes.buscar(anterior);
					//Se hace el cambio, donde esta el objeto se pone el nuevo si se cumplio la condicion anunciada dos lineas m�s arriba
					listaViajesMes.remplazar(listaViajesMes.buscar(anterior), listaViajesMes.buscar(j));
					listaViajesMes.remplazar(listaViajesMes.buscar(j), aux);
				}
			}
		}
		return listaViajesMes;
	}
	/**
	 * Este metodo concatena en un String las caracterizticas de cada uno de los objetos de una lista que contiene los viajes de uber 
	 * que cumplen con todos los requisitos que nos pide el punto 1B
	 * @param listaViajes
	 * @return resp devuelve un string con todos los viajes uber que cumplen con las caracterizticas
	 */
	public String respuesta2A(IListaEnlazada<viajeUberMes> listaViajes) {
		String resp="";

		/*
		 * 1. recorrer la lista
		 * 	1.1 recuperar los datos dl viaje
		 * 	1.2 meter los datos relevantes al string resp
		 */
		for (int i = 0; i < listaViajes.size(); i++) {//1
			viajeUberMes actual = listaViajes.buscar(i); //1.1
			resp+="El viaje uber numero " + (i+1) + "tiene un tiempo promedio "+ actual.darmeanTravelTime() +
					"y su desviacion es " + actual.darstandardDeviationTravelTime()+ ", su zona origen "+actual.darsourceId()+ ", su zona destino es "+actual.dardestinID()+ "\n";//1.2
		}

		return resp;
	}
	/**
	 * el metodo funcion2A se encarga de unir todos los metodos antes estipulados, esto quiere decir que es el metodo "madre"
	 * puesto que une todos los metodos anteriores que se encargan de cumplir con los requerimientos de punto 2A y lo retorna en un string
	 * @param mes
	 * @param N
	 * @return respuesta contiene todos los viajes uber con las caracterizticas estipuladas en el punto 2
	 */
	public String funcion2A(int mes,int N) {
		IListaEnlazada<viajeUberMes> viajesPorMes=darViajesPorMes(mes);
		IListaEnlazada<viajeUberMes> viajesResultado=ordenarPorTiempo(viajesPorMes);
		String respuesta=respuesta2A(viajesResultado);
		return respuesta;
	}
	/**
	 * Este metodo saca de la lista establecida por el mes dado por parametro, los viajes que cumplen con el rango dado por parametro, y los que contienen
	 * como inicio o como final la zonaDada	
	 * @param zonaDada
	 * @param zonaMenor
	 * @param zonaMayor
	 * @param listaViajes
	 * @return listaRangoEstablecido la cual contiene todos los viajes entre zonaMenor y zonaMayor y que contienen como 
	 * destino o como incio la zona dada por parametro
	 */
	public IListaEnlazada<viajeUberMes> zonaX (int zonaDada,int zonaMenor, int zonaMayor, IListaEnlazada<viajeUberMes> listaViajes ){
		//Creacion de lista vacia
		IListaEnlazada<viajeUberMes> listaRangoEstablecido = new ListaEnlazada<viajeUberMes>();
		//Recorremos la lista dada por el metodo darViajesPorMes
		for (int i = 0; i < listaViajes.size(); i++) {
			//Creamos una variable que guarde el objeto actual
			viajeUberMes actual = listaViajes.buscar(i);
			//comparamos el actual con el rango establecido y vemos si el actual en su inicio o su final es  zonaDada
			if((actual.dardestinID()== zonaDada)&&(actual.darsourceId()==zonaMenor|| actual.darsourceId()>zonaMenor)&& (actual.darsourceId()==zonaMayor||actual.darsourceId()<zonaMayor)
					|| (actual.darsourceId()==zonaDada)&&(actual.dardestinID()== zonaMayor||actual.dardestinID()<zonaMayor)&&(actual.dardestinID()==zonaMenor||actual.dardestinID()>zonaMenor))
			{
				//Si cumple las condiciones metemos el actual dentro de la clase creada
				listaRangoEstablecido.agregar(actual);
			}
		}
		//retorna la lista con los viajesUber que contenga el mes dado por parametro en el metodo darViajesPorMes y el rango
		return listaRangoEstablecido;
	}

	/**Este metodo se encarga de unificar todos los metodos de 
	 * la parte 3A
	 * 
	 */
	public String respuesta3A(IListaEnlazada<viajeUberMes> listaViajes) {
		String resp="";
		/*
		 * 1. recorrer la lista
		 * 	1.1 recuperar los datos del viaje
		 * 	1.2 meter los datos releantes al string resp
		 */
		for (int i = 1; i < listaViajes.size(); i++) {//1
			viajeUberMes anterior = listaViajes.buscar(i-1);
			viajeUberMes actual = listaViajes.buscar(i); //1.1
			resp+="El  tiempo promedio es "+ anterior.dargeometricMeanTravelTime() +
					"de " + anterior.darsourceId()+ "a "+anterior.dardestinID()+ "vs "+actual.dargeometricMeanTravelTime()+"de"+actual.darsourceId()+ " a "+actual.dardestinID()+ "\n";//1.2
		}
		return resp;
	}

	/**
	 * El metodo se encarga  funcion3A se encarga de unir todos los metodos para cumplir los requisitos
	 * @param zonaMinima
	 * @param zonaMaxima
	 * @param mes
	 * @param zonaDada
	 * @return
	 */
	public String funcion3A(int zonaMinima,int zonaMaxima,int mes, int zonaDada ){
		String viajesFinales = "";

		/*
		 * 1. Filtrar por mes
		 * 2. Filtrar por zonaDada, zonaMinima, zonaMaxima y viajesMes (recibe lo que saque 1)
		 * 3. toString (recibe lo que saque 2)
		 * 4. return 3
		 * 
		 */
		IListaEnlazada<viajeUberMes> viajesMes=darViajesPorMes(mes);
		IListaEnlazada<viajeUberMes> viajesConInDesMes=zonaX(zonaDada,zonaMinima, zonaMaxima, viajesMes);
		viajesFinales= respuesta3A(viajesConInDesMes);		
		return viajesFinales;
	}

}
